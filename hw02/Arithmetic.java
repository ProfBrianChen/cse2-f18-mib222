//Mickias Bekele; September 9,2018; CSE002
//The purpose of the program is to calculate and compute: /*Total cost of each kind of item (i.e. total cost of pants, belts and sweatshirts)
                                                          /*Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts, pants and sweatshirts)
                                                          /*Total cost of purchases of the items (before tax)
                                                          /*Total sales tax
                                                          /*Total paid for this transaction, including sales tax.*/



public class Arithmetic {
          //main method is required for every Java program
  
  public static void main(String args[]){
          //our input data with variables
    
          int numPants = 3;                                                             //input value for the number of pairs of pants
          double pantsPrice = 34.98;                                                    //input value for the cost per pair of pants
          int numShirts = 2;                                                            //input value for the number of sweatshirts
          double shirtPrice = 24.99;                                                    //input value for the cost per shirt
          int numBelts = 1;                                                             //input value for the number of belts
          double beltCost = 33.99;                                                      //input value for the cost per belt
          double paSalesTax = 0.06;                                                     //input value for the tax rate
          double totalCostOfPants, totalCostOfBelts, totalCostOfShirts;                 //values stored for the total cost of pants, belts and sweatshirts respectively
          double salesTaxOnBelts, salesTaxOnPants, salesTaxOnShirts;                    //values stored for the sales tax charged buying all of each kind of item (belts, pants and shirts)
          double totalCostBeforeTax, totalSalesTax, totalCostAfterTax;                  /*output variables for the total cost of all items (before tax), total sales tax, total cost of all 
                                                                                         items after tax*/
          
           //calculating the values assigned
    
          totalCostOfPants = numPants * pantsPrice;                                     //gives us the total cost of pants
          totalCostOfBelts = numBelts * beltCost;                                       //gives us the total cost of belts
          totalCostOfShirts = numShirts * shirtPrice;                                   //gives us the total cost of the sweatshirts
          salesTaxOnBelts = totalCostOfBelts * paSalesTax;                              //gives us the sales charged on the belts
          salesTaxOnPants = totalCostOfPants * paSalesTax;                              //gives us the sales charged on the pants
          salesTaxOnShirts = totalCostOfShirts * paSalesTax;                            //gives us the sales charged on the sweatshirts
          totalSalesTax = salesTaxOnBelts + salesTaxOnShirts * salesTaxOnPants;         //gives us the total sales tax charged on all items
          totalCostBeforeTax = totalCostOfShirts + totalCostOfBelts + totalCostOfPants; //gives us the total cost of all items before tax
          totalCostAfterTax = totalCostBeforeTax + totalSalesTax;                       //gives us the total cost of all the items after tax
   
          totalSalesTax = totalSalesTax * 100;                                          //in order to make the price have only two decimals, we multiply it by 100
          totalSalesTax = (int) totalSalesTax;                                          //cast the double value into integer
          totalSalesTax = totalSalesTax / 100;                                          //finaly divide the value by 100 so as to make the price have only two decimals
    
          totalCostAfterTax = totalCostAfterTax * 100;                                  //in order to make the price have only two decimals, we multiply it by 100
          totalCostAfterTax = (int) totalCostAfterTax;                                  //cast the double value into integer
          totalCostAfterTax = totalCostAfterTax / 100;                                  //finaly divide the value by 100 so as to make the price have only two decimals
                                                     
          
          //print out the output values
          
          System.out.println("The total price of all the items bought before tax is $" + totalCostBeforeTax);        //outputing the result for the total cost of all items before tax 
          System.out.println("The total sales tax charged on all the items is $" + totalSalesTax);                   //output for the total sales tax charged on all the items
          System.out.println("The total cost of all the items bought (including the tax) is $" + totalCostAfterTax); //output for the total cost of all the items including tax
    
    
    
          
      

    
    
    
  }
  
}