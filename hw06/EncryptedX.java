//Mickias Bekele; September 7, 2018; CSE02
//HW 06 EncryptedX.java 
/*The purpose of the program to print out a secret encrypted message X. Instead showing X to the user, we will
hide it in stars.*/
import java.util.Scanner; //use Scanner if the user wants to input the roll of the two dices

public class EncryptedX{
//main method required for every Java program
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);  //assigning scanner variable for the user to input
    boolean checkInput = true;  //boolean varialbe to check whether or not the user entered the correct input
    System.out.println("Enter an integer between 0-100");  //asking the user to input an integer between 0-100
    checkInput = myScanner.hasNextInt();  //checking if the user entered an integer or not
    //we use while loop to keep asking the user if they did not input an integer until they enter an integer
    while(checkInput != true){  
    System.out.println("Wrong input. Enter again");
    myScanner.next();
    checkInput = myScanner.hasNextInt();
    }
    int x = myScanner.nextInt();  //we assign a variable for the inputted integer
    //the integer should be between 0-100
    //therefore, in case the user entered a number out of that range, we use while loop to keep asking the user to input a number between 0-100
    while(x > 100 || x < 0){
    System.out.println("Wrong Input. Enter again");
    myScanner.next();
    x = myScanner.nextInt();
    }
    //after this, we use nested loop to output the secret message
    for(int i = 1; i <= x+1; ++i){  //we use this for the rows
      for(int j = 1; j <= x+1; ++j){  //we use this for the columns
        if(j == i){  //we use if statements to print out the X message, which in this case is " "
        System.out.print(" "); //we use this to print out on the first side of the square 
        }
        else if(j == (x+2) - i){ //we use this to print out on the other side
        System.out.print(" ");
        }
        else{   //we use this to print out the rest of the pattern or stars
        System.out.print("*");
        }
      }
      System.out.println();
    }
     
    
    
    
  }//end of main method
}//end of class