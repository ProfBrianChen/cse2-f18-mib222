//Mickias Bekele; September 7, 2018; CSE02
//HW 03 Pyramid.java
/*The purpose of the program is generate five cards (hands) and try to give probability for each type of hands.*/
//use first import method otherwise the compiler will generate error messages.
import java.util.Scanner; //use Scanner if the user wants to input the roll of the two dices
import java.util.Random;  //use Random if the user wants the computer to generate the number
public class Hw05 {
    //main method is required for every Java Program
    public static void main(String args[]) {
        Scanner scnr = new Scanner(System.in);  //new Scanner object
        Random randGen = new Random();
        boolean numTimes = true;  //boolean variable for checking whether the user inputted integer or not
        int i = 0, card1 = 0, card2 = 0, card3 = 0, card4 = 0, card5 = 0; //variables for the cards 
        double fourHands = 0, threeHands = 0, twoHands = 0, oneHand = 0;  //variables for the types of hands
        double probability1, probability2, probability3, probability4;    //output variable for probability of the hands
        double sum;
        System.out.println("Enter how many times the hands to be generated"); //asking the user input how many times the hands to be generated
        numTimes = scnr.hasNextInt();  //checking whether or not the user inputted integer or not
        //we use while loops to keep asking the user until they input integer
        while(numTimes != true){
          System.out.println("Enter again. The value you entered is not valid");
          scnr.next();
          numTimes = scnr.hasNextInt(); //checking the user whether or not the user entered integer
       }
      numTimes = false;
          int x = scnr.nextInt();  //variable assigning number being inputted
      while(i < x){
             card1 = (randGen.nextInt(52) + 1) % 13;   //for generation of the first card
             card2 = (randGen.nextInt(52) + 1) % 13;   //for generation of the second card
             card3 = (randGen.nextInt(52) + 1) % 13;   //for generation of third card
             card4 = (randGen.nextInt(52) + 1) % 13;   //for generation of the fourth card
             card5 = (randGen.nextInt(52) + 1) % 13;   //for generation of the fifth card
             if(card1 == card2 && card1 == card3 && card1 == card4 && card1 != card5){
                  fourHands = fourHands + 1;           //for the fourth-of-a-kind
                 }
             else if((card1 == card2) && (card1 == card3) && (card1!= card4) && (card1 != card5)){
                  threeHands++;                        //for the third-of-a-kind
                  if(card4 == card5){
                  oneHand++;                           //for full house for one pair and three-of-a-kind
                  }                
                }
             else if((card1 == card2) && (card1 != card3) && (card1 != card4) && (card1 != card5) && (card2 == card3)){
                 twoHands++;                           //for two pairs
                }
             else if((card1 == card2) && (card1 != card3) && (card1 != card4) && (card1 !=card5)){
                 oneHand++;                           //for one pair
                }
              i++;   
            }
          sum = fourHands + threeHands + twoHands + oneHand;       //sum of all the hands total hands
          probability1 = fourHands/sum;                            //probability for the four-of-a-kind
          probability2 = threeHands/sum;                           //probability for the three-of-a-kind
          probability3 = twoHands/sum;                             //probability for the two pair
          probability4 = oneHand/sum;                              //probability for the one pair
           //print out the output
          System.out.println("The number of loops " + x);         //print out the number of loops
          System.out.printf("The probability of Four-of-a-kind: %3.3f \n", probability1);   //the probability for the four-of-a-kind
          System.out.printf("The probability of Three-of-a-kind: %3.3f \n", probability2);  //the probability for the three-of-a-kind
          System.out.printf("The probability of Two-pair: %3.3f \n", probability3);         //the probability for the two pair
          System.out.printf("The probability of One-pair: %3.3f \n", probability4);         //the probability for one pair
      }//end of main method 
        }//end of class
    
        
        
    
        
        
        
        
       
    
    


