//Mickias Bekele; September 7, 2018; CSE02
//HW 06 EncryptedX.java 
/*The purpose of the program is to demand the user to input a text so that the computer can do what the user chooses from the options
We achieve this by using methods*/
import java.util.Scanner;//import Scanner class
import java.util.regex.Pattern;//import Pattern class
import java.util.regex.Matcher;//import Matcher class
public class WordTools{
  //main method required for every Java prog
 public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //assigning Scanner variable
    String text = sampleText(myScanner);   //calling the method sampleText and storing the value in the string text
    char c = printMenu(text);              //call the printMenu to show the options and store the option the user enters
    System.out.println(c);                  
    if(c == 'c'){ //if the user enters c
       System.out.println("Number of White spaces characters: " + getNumOfNonWSCharacters(text)); //calling getNumOfNonWSCharacters
  }
    if(c == 'w'){ //if the user enters w
       System.out.println("The number of words: " + getNumOfWords(text)); //calling getNumOfWords
  }
    if(c == 'f'){ //if the user enters f
        System.out.println("Enter the word you want to search: "); 
        String wordFinder = myScanner.nextLine();   //assigning the value for the word the user had inputted
       System.out.println("The number of occurrences of the word " + wordFinder + " is " + findText(wordFinder, text) + " times");  //calling the findText method
  }
    if(c == 'r'){ //if the user enters r
       System.out.println("Edited text: " + replaceExclamation(text)); //calling replaceExclamation method
  }
    if(c == 's'){  //if the user enters s
       System.out.println("Edited text: " + shortenSpace(text)); //calling the shortenSpace method
  
  }
   if(c == 'q'){
      System.out.println("GoodBye"); //if the user enter q for quit 
    }
  }
 
  public static String sampleText(Scanner myScanner){//add sampleText method 
  //this sampleText method is used to ask the user to enter a sentence and output the sentence into the main method
    System.out.println("Enter a sample text:");//prompting user for the sample text
  String text = myScanner.nextLine();
  return text;//taking in the text and retunring it in main method
 }//end of sampleText method
  
  public static char printMenu(String text2){// printMenu method
    Scanner myScanner = new Scanner(System.in); //creating a scanner object
    System.out.println("You Entered: " + text2); //reminding the user of the sentences put 
    System.out.println("\nMENU\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit\nChoose an option:");//prompting the user for response on their stupid text
   char textInput = myScanner.next().charAt(0);//taking in the response
   while(!(textInput == 'c'  || textInput == 'w' || textInput == 'f' || textInput == 'r' || textInput == 's' || textInput == 'q')){ //if the user inputs other options
   System.out.println("Invalid option"); 
   System.out.println("\nMENU\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit\nChoose an option:");//prompting the user to input a response
   myScanner.nextLine();
   textInput = myScanner.next().charAt(0);//taking in the response again
  }
   return textInput; //taking in the user's response and returning it to main method
 }//end of printMenu method

 public static int getNumOfNonWSCharacters(String nonWhiteSpace){//add getNumOfNonWSCharacters method
  int word = 0;//count for the number of non white space characters
  for(int i = 0; i < nonWhiteSpace.length(); ++i){//going through all the index in the string
   if(!(Character.isWhitespace(nonWhiteSpace.charAt(i)))){
    word++;//incrementing the count for every non white space character
   }
   else{
     continue;
   }
  }
  return word;//returning to main method
 }//end of getNumOfNonWSCharacters method

 public static int getNumOfWords(String numWords){//add getNumOfWords method
  //to find the number of words inputted
  int word = 0;//count for number of words
  numWords = numWords.trim().replaceAll(" +"," "); //relacing all the double spaces and more spaces with only one space
  
  for(int i = 0; i<numWords.length(); ++i){ //going through all the index in the string
    if(Character.isWhitespace(numWords.charAt(i))){  //counting all the white spaces
      word++; //incrementing
    }//end of if statement   
    }//end of for loop
     word++; //finaly increasing the incremented by one since the number of words is greater than number of white spaces 
   return word;    //return the number of words
}//end of getNumOfWords
 public static int findText(String input1, String input2){  //findText method
  //to find the number of instances of inputted word in the sentence
   int i = 0;  
   Pattern p = Pattern.compile(input1); //pattern class
   Matcher m = p.matcher(input2);  //matcher class
   while(m.find()){ //finding the desired word
     i++; //incrementing 
   }
   return i; //returning the number of words
  }//end of findText method
 public static String replaceExclamation(String replace){ //replaceExclamation method
   //to replace all words with exclamation with period
   String replaceExclamation = replace.replace("!", "."); //replacing all ! by .
   return replaceExclamation; //returning the number
 }//end of replaceExclamation method
 public static String shortenSpace(String shorten){  //shortenSpace method
   //replacing all spaces with a single space
   shorten = shorten.trim().replaceAll(" +"," "); //replacing all the spaces with single space 
    return shorten;  //returning the value
   
 }//end of shortenSpace method
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
}