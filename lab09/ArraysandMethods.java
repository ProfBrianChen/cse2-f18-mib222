public class ArraysandMethods{
//main method required for every java program
  public static void main(String[] args){
  
    int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
    int[] array1 = {0, 9, 8, 7, 6, 5, 4, 3};
    int[] array2=  {3, 4, 5, 6, 7, 8, 9, 0};
    int[] array5 = inverter(array0);
    print(array5);
    System.out.println();
    int[] array7 = inverter2(array1);
    print(array7);
    System.out.println();
    int[] array3 = inverter(array2);
    print(array3);
    
    
  
  }//end of main method
  
  public static int[] copy(int[] array){
    int[] array1 = new int[8];
    for(int i = 0; i < 8; ++i){
      array1[i] = array[i];
    }//end of for loop
    return array1;
  }//end of copy method
  
  public static int[] inverter(int[] array){
    int temp;
    int[] array0 = new int[8];
    for(int i = 7; i >= 0; --i){
     array0[7-i] = array[i];
    }//end of for loo
    return array0;
  
  }//end of inverter method
  
  public static int[] inverter2(int[] array){
     int[] array1 = copy(array);
     int [] array4 = new int[8];
     for(int i = 7; i >= 0; --i){
       array4[7-i] = array[i];
    }//end of for loop
    return array4;
    
  }//end of inverter2 method
  
  public static void print(int[] array){
    for(int i = 0; i < 8; ++i){
      System.out.print(array[i] + " ");
    }//end of for loop
  }//end of print method

}//end of class