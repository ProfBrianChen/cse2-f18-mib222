//Mickias Bekele; September 7, 2018; CSE02
//Lab 06 PatternA.java
/*The purpose of the program is to output a pattern of matrix from the range of 1-10 by asking the user
to input an integer number from 1-10 using nested loops*/

import java.util.Scanner;
public class PatternC{
  //main method required for every Java Program
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); ////assigning scanner variable
    boolean rangeNum3 = true;
    System.out.println("Enter a number from 1-10 for number of lines");
    rangeNum3 = myScanner.hasNextInt();
    
    while(rangeNum3 != true){
       //checking if the user inputted the right type of input
      System.out.println("Out of range.Enter again");
      myScanner.next();
      rangeNum3 = myScanner.hasNextInt();
    }
    int numTimes3 = myScanner.nextInt();
    while(numTimes3 > 10 || numTimes3 < 0){ //checking if the user inputted an integer in the range
    System.out.println("Out of Range. Enter again");
     myScanner.next();
     numTimes3 = myScanner.nextInt();
    }
    for(int numRows = 1; numRows <= numTimes3; ++numRows){ // printing out the patterns
      for(int spaceIndent = numTimes3 - 1; spaceIndent >= numRows; --spaceIndent){
      System.out.print(" ");
      }
      for(int numColumns = numRows; numColumns >= 1; --numColumns){
      System.out.print(numColumns + "");
      }
      
        
      System.out.println();
    }
   
      
    
  }//end of main method
  
}//end of class