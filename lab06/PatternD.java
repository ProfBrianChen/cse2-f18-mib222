//Mickias Bekele; September 7, 2018; CSE02
//Lab 06 PatternA.java
/*The purpose of the program is to output a pattern of matrix from the range of 1-10 by asking the user
to input an integer number from 1-10 using nested loops*/

import java.util.Scanner;
public class PatternD{
  //main method required for every Java Program
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); ////assigning scanner variable
    boolean rangeNum2 = true;
    System.out.println("Enter a number from 1-10 for number of lines");
    rangeNum2 = myScanner.hasNextInt(10);
    while(rangeNum2 != true){
       //checking if the user inputted the right type of input
    System.out.println("Out of range.Enter again");
    myScanner.next();
    rangeNum2 = myScanner.hasNextInt(11);
    }
    int numTimes2 = myScanner.nextInt();
    while(numTimes2 > 10 || numTimes2 < 0){ //checking if the user inputted an integer in the range
      System.out.println("Out of range. Enter again");
      myScanner.nextInt();
      numTimes2 = myScanner.nextInt();
    }
    for(int numRows = numTimes2; numRows >= 1; --numRows){ // printing out the patterns
      for(int numColumns = numRows; numColumns >= 1; --numColumns){
      System.out.print(" " + numColumns);
      }
      
      System.out.println();
    }
   
      
    
  }//end of main method
  
}//end of class