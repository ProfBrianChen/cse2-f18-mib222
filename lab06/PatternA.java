//Mickias Bekele; September 7, 2018; CSE02
//Lab 06 PatternA.java
/*The purpose of the program is to output a pattern of matrix from the range of 1-10 by asking the user
to input an integer number from 1-10 using nested loops*/

import java.util.Scanner;
public class PatternA{
  //main method required for every Java Program
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //assigning scanner variable
    boolean rangeNum = true;
    System.out.println("Enter a number from 1-10 for number of lines");
    rangeNum = myScanner.hasNextInt(10);
    while(rangeNum != true){ 
      //checking if the user inputted the right type of input
    System.out.println("Out of range.Enter again");
    myScanner.next();
    rangeNum = myScanner.hasNextInt(11);
    }
    int numTimes = myScanner.nextInt();
    while(numTimes > 10 || numTimes < 0){ //checking if the user inputted an integer in the range
      System.out.println("Out of range. Enter again");
      myScanner.next();
      numTimes = myScanner.nextInt();
    }
    for(int numRows = 1; numRows <= numTimes; ++numRows){ // printing out the patterns
      for(int numColumns = 1; numColumns <= numRows; ++numColumns){
      System.out.print(" " + numColumns);
      }
      
      System.out.println();
    }
   
      
    
  }//end of main method
  
}//end of class