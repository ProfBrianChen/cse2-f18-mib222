public class WelcomeClass{
  //main method required for every java program
  
  public static void main(String args[]){
    
    System.out.println("-----------"); //prints first line
  
    System.out.println("| Welcome |"); //prints second line
    System.out.println("-----------"); //prints third line
    System.out.println(" ^  ^  ^  ^  ^  ^"); //prints fourth line
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\"); //prints fifth line
    System.out.println("<-M--I--B--2--2--2->"); //prints sixth line
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /"); //prints seventh line
    System.out.println(" v  v  v  v  v  v");  //prints eighth line
    System.out.println("My name is Mickias, and I am a first year computer science student. I am from Ethiopia, and in my spare time"); //prints my firs line of my bio
    System.out.println("I like coding, playing soccer and hiking.");  //prints my last line of bio 
    
   
  }//end of main method
  
}//end of class
  