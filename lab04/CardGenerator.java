//Mickias Bekele,  September 7, 2018; CSE02

//Lab 04 Card generator program

//The purpose of this program is to pick or generate a random card from the deck so a person can practice the  tricks alone. 

//use first the import method or otherwise the compiler will generate error messages

import java.util.Scanner;

  
public class CardGenerator{
   // main method is required for every Java program
  public static void main(String args[]){
   
    String suitName = "Error";                       //output variable for the suit of the card
   
    String royalCard = "Stupid";                //output variable for the identity of the card
    
    int cardGenerator = (int)(Math.random() * 51) + 1; //equation for the calculation of the random generating of numbers
    
    //Then we use the if statement to display the output depending on the number of that is generated
    
    if(1 < cardGenerator && cardGenerator <= 52){
   
      if(cardGenerator <= 13){   //if the card generated is between 1 - 13
      
       suitName = "Diamonds";
        
        switch (cardGenerator) {
          case 1: 
            royalCard = "Ace";
            break;
          case 2: 
            royalCard = "2";
            break;
          case 3 : royalCard = "3";
            
          case 4: royalCard = "4";
           
            break;
          case 5: royalCard = "5";
            
            break;
          case 6: royalCard = "6";
            
            break;
          case 7: royalCard = "7";
            
            break;
            
          case 8: royalCard = "8";
           
            break;
          case 9: royalCard = "9";
           
            break;
          case 10: 
            royalCard = "10";
            
            break;
          case 11: 
            royalCard = "Jack";
           
            break;
          case 12: 
            royalCard = "Queen";
            
            break;
          case 13: royalCard = "King";
           
            break;
         
        }
        
    }
      
           
                              
            
    
    
    
       else if(13 < cardGenerator && cardGenerator <= 26 ){
        
        suitName = "clubs";
        
        switch (cardGenerator){
          case 14 : royalCard = "Ace";
            
           
            break;
          case 15: royalCard = "2";
            
            break;
          case 16: royalCard = "3";
           
            
            break;
          case 17: royalCard = "4";
            
            
            break;
          case 18: royalCard = "5";
           
            
            break;
          case 19: royalCard = "6";
            
            
            break;
            
          case 20: royalCard = "7";
            
            
            break;
          case 21: royalCard = "8";
            
            break;
          case 22: royalCard = "9";
            
            
            break;
          case 23: royalCard = "10";
           
            
            break;
          case 24: royalCard ="Jack";
            
            break;
          case 25: royalCard = "Queen";
            
            break;
          case 26: royalCard ="King";
           
            break;
          
                      
        }
       
                              
            
        
        
      }
    
       else if(26 < cardGenerator && cardGenerator <= 39){
        
        suitName = "hearts";
        
        switch(cardGenerator){
            
          case 27: royalCard = "Ace";
           
            
            break;
            
          case 28: royalCard = "2";
            
            break;
            
          case 29: royalCard = "3";
            
            
            break;
          case 30: royalCard = "4";
           
            break;
          case 31: royalCard = "5";
            
            break;
          case 32: royalCard = "6";
            
            
            break;
          case 33: royalCard = "7";
           
            break;
          case 34: royalCard = "8";
            
            break;
          case 35: royalCard = "9";
           
            break;
          case 36: royalCard = "10";
            
            break;
          case 37: royalCard = "Jack";
           
            break;
          case 38: royalCard = "Queen";
            
            break;
          case 39: royalCard = "King";
           
            break;
            
        }
        
        
      }
     
                              
               
      
      else if(39 < cardGenerator && cardGenerator <= 52 ){
        
        suitName = "spade";
        
        switch (cardGenerator){
            
          case 40: royalCard = "Ace";
            
            break;
            
          case 41: royalCard = "2";
            
            break;
            
          case 42: royalCard = "3";
           
            break;
            
          case 43: royalCard = "4";
            
            break;
          case 44: royalCard = "5";
           
            break;
          case 45: royalCard = "6";
           
            break;
          case 46: royalCard = "7";
           
            break;
          case 47: royalCard = "8";
           
            break;
          case 48: royalCard = "9";
            break;
          case 49: royalCard = "10";
           
            break;
          case 50: royalCard = "Jack";
            
            break;
          case 51: royalCard ="Queen";
            
            break;
          case 52: royalCard ="King";
            
            break;
          
            
        }
        
        
      }
    }
    
      
      System.out.println("You picked the " + royalCard + "of" + suitName);
          
      
      
      
      
      
    
    
    
    
    
    
    
    
    
    
}//end of main method 
    
    
  }//end of class
      