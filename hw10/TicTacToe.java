//import object or it will compile error
import java.util.Scanner; //import scanner

public class TicTacToe{
//main method required for every Java method
  public static void main(String[] args){
  Scanner myScanner = new Scanner(System.in); //declaring scanner variable
  int key; //declaring integer the user enters
  int i;
  int j;
  int a = 1; //declaring integer
  final int NUM_ROWS = 3; //assigning the number of rows to 3
  final int NUM_COLS = 3; //asssigning the number of columns to 3
  boolean checkInt = true;
  String[][] myArray = new String[NUM_ROWS][NUM_COLS]; //declaring and assigning a new array with the size of 3x3
  //giving the new  array a position
  myArray[0][0] = "1";
  myArray[0][1] = "2";
  myArray[0][2] = "3";
  myArray[1][0] = "4";
  myArray[1][1] = "5";
  myArray[1][2] = "6";
  myArray[2][0] = "7";
  myArray[2][1] = "8";
  myArray[2][2] = "9";
  for(i = 0; i < NUM_ROWS; ++i){// for printing it out
    for( j = 0; j < NUM_COLS; ++j){
      System.out.print(myArray[i][j] + " ");
    }//end of for loop
    System.out.println();
  }//end of for loop
  while(a <= 9){ //until one of the players win or there is a draw
    if(a ==1){
      System.out.println("Player 1 move");
    }//end of if statement
    System.out.println("Enter the position"); //asking the player to enter the position
  checkInt = myScanner.hasNextInt();
  while(checkInt != true){ //if the input is not integ
    System.out.println("Error enter again");
    myScanner.next();
    checkInt = myScanner.hasNextInt();
    
  }//end of while loop
  key = myScanner.nextInt();
  while(key < 1 || key > 9){ //if the number entered out of range
    System.out.println("Error. enter again");
    myScanner.next();
    key = myScanner.nextInt();
  }//end of while loop
   
  if(a%2 == 1){ //if statement for the first player
  myArray = newArray(myArray, key); //calling the newArray method that generate o on the position selecte
  for(i = 0; i < NUM_ROWS; ++i){ //prints out the new array
    for(j = 0; j < NUM_COLS; ++j){
      System.out.print(myArray[i][j] + " ");
    }//end of for loop
    System.out.println();
  }//end of for loop
  System.out.println("Player 2 move");
  
  }//end of if statement
  else if(a%2 == 0){ //if statemen for the second player
    myArray = newArray2(myArray,key); //calling the newArray2 method to generate x on the position selected
    for(i = 0; i < NUM_ROWS; ++i){//prints out the new array
      for(j = 0; j < NUM_COLS; ++j){
      System.out.print(myArray[i][j] + " ");
    }//end of for loop
    System.out.println();
  }//end of for loop
    System.out.println("Player 1 move");
  }//end of if statement
  //we use if statements below to check whether the player won or not
  //we use break to break of the loop and determine which player won 
  if(myArray[0][0]==myArray[1][0] && myArray[1][0] == myArray[2][0] && (myArray[0][0]== "x" || myArray[0][0]== "o")){
                break;
  }
  else if(myArray[0][1]==myArray[1][1] && myArray[1][1] == myArray[2][1] && (myArray[0][1]=="x" || myArray[0][1]=="o")){
                break;
  }
  else if(myArray[0][2]==myArray[1][2] && myArray[1][2] == myArray[2][2] && (myArray[0][2]== "x"|| myArray[0][2]== "o")){
                 break;
  }
  else if(myArray [0][0]== myArray[0][1] && myArray[0][1] == myArray[0][2]&& (myArray [0][0]=="x"|| myArray [0][0]== "o")){
                 break;
  }
  else if( myArray[1][0]== myArray[1][1] && myArray[1][1] == myArray[1][2] && (myArray[1][0]=="x" || myArray [1][0]== "o")){
                break;
  }
  else if( myArray[2][0]== myArray[2][1] && myArray[2][1] == myArray[2][2] && (myArray[2][0]=="x" || myArray [2][0]== "o")){
                break;
  }
  else if( myArray[0][0]== myArray[1][1] && myArray[1][1] == myArray[2][2] && (myArray[0][0]== "x"|| myArray [0][0]=="o")){
                 break;
  }
  else if(myArray[2][0]== myArray[1][1] && myArray[1][1] == myArray[0][2] && (myArray [2][0]== "x" || myArray[2][0]== "o")){
                break;
  }
    a++;
  }//end of while loop
  if(a%2 == 0){ //if the second player won
    
    System.out.println("Player 2 has won");
  }//end of if statement
  else if(a < 9 && a%2 == 1){ //if the player 1 won
    System.out.println("Player 1 has won");
  }//end of if statement
  else if(a ==9){ //if there is a draw
    System.out.println("It's a DRAW");
  }
  }//end of main method
  public static String[][] newArray(String[][] myArray, int key){ //newArray method
    switch(key){ //switch for replacing the position selected with o
      case 1: 
        if(myArray[0][0] == "o" || myArray[0][0] == "x"){ //checking if the posititon is already taken 
        break;
      }
        else{ myArray[0][0] = "o";
        }
      break;
      case 2:  if(myArray[0][1] == "o" || myArray[0][1] == "x"){
        break;
      }
        else{ myArray[0][1] = "o";
        }
      break;
      case 3: if(myArray[0][2] == "o" || myArray[0][2] == "x"){
        break;
      }
        else{ myArray[0][2] = "o";
        }
      break;
      case 4:  if(myArray[1][0] == "o" || myArray[1][0] == "x"){
        break;
      }
        else{ myArray[1][0] = "o";
        }
      break;
      case 5: if(myArray[1][1] == "o" || myArray[1][1] == "x"){
        break;
      }
        else{ myArray[1][1] = "o";
        }
      break;
      case 6:  if(myArray[1][2] == "o" || myArray[1][2] == "x"){
        break;
      }
        else{ myArray[1][2] = "o";
        }
      break;
      case 7:  if(myArray[2][0] == "o" || myArray[2][0] == "x"){
        break;
      }
        else{ myArray[2][0] = "o";
        }
      break;
      case 8:  if(myArray[2][1] == "o" || myArray[2][1] == "x"){
        break;
      }
        else{ myArray[2][1] = "o";
        }
      break;
      case 9:  if(myArray[2][2] == "o" || myArray[2][2] == "x"){
        break;
      }
        else{ myArray[2][2] = "o";
        }
      break;
      default: 
    }//end of switch 
    return myArray; //returns the new array
  }//end of newArray
  public static String[][] newArray2(String[][] myArray, int key){ //newArray2 method
    switch(key){//switch for replacing the position selected with x
      case 1: 
         if(myArray[0][0] == "o" || myArray[0][0] == "x"){ //checking if the position is already taken 
        break;
      }
        else{ myArray[0][0] = "x";
        }
      break;
      case 2: if(myArray[0][1] == "o" || myArray[0][1] == "x"){
        break;
      }
        else{ myArray[0][1] = "x";
        }
      break;
      case 3:  if(myArray[0][2] == "o" || myArray[0][2] == "x"){
        break;
      }
        else{ myArray[0][2] = "x";
        }
      break;
      case 4: if(myArray[1][0] == "o" || myArray[1][0] == "x"){
        break;
      }
        else{ myArray[1][0] = "x";
        }
      break;
      case 5: if(myArray[1][1] == "o" || myArray[1][1] == "x"){
        break;
      }
        else{ myArray[1][1] = "x";
        }
      break;
      case 6:  if(myArray[1][2] == "o" || myArray[1][2] == "x"){
        break;
      }
        else{ myArray[1][2] = "x";
        }
      break;
      case 7:  if(myArray[2][0] == "o" || myArray[2][0] == "x"){
        break;
      }
        else{ myArray[2][0] = "x";
        }
      break;
      case 8:  if(myArray[2][1] == "o" || myArray[2][1] == "x"){
        break;
      }
        else{ myArray[2][1] = "x";
        }
      break;
      case 9:  if(myArray[2][2] == "o" || myArray[2][2] == "x"){
        break;
      }
        else{ myArray[2][2] = "x";
        }
      break;
      default: 
    }//end of switch 
    return myArray; //returns the new array
  }//end of newArray2 method

}//end of class