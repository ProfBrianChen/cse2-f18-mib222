//Mickias Bekele; September 7, 2018; CSE02
//Lab 05 Lab05.java
/*The purpose of the program is to ask the user to input values corresponding to the course number, department, how many times a week
they are attending, what time it starts, the instructor's name and the number of students*/

//use import method otherwise it will compile error
import java.util.Scanner;

public class Lab05{
  //main method is required for every Java Program
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);  //assigning the variable myScanner for inputting
    
    boolean numStudents = true;   //input variable assigning truth value for number of students
    int inputNum = 0;             //input variable assigning for the option for the user to input the values or to quit
    boolean courseNum = true;     //input variable boolean for the cours number
    boolean numTimes = true;      //input variable for the number of times the user attends class
    boolean instName = true;      //input variable for the instructor's name
    boolean timeStarts = true;    //input variable for the time it starts
    boolean depName = true ;      //input variable for the department number
    System.out.println("Enter 1 to input the numbers or 2 to quit");  //asking the user whether to input the values or to quit
       inputNum = myScanner.nextInt();
    
    //we use while loop to constantly ask the user input for the course or to quit
    while(inputNum == 1){    
       
      System.out.println("Enter the course number: ");   //for the course number
      courseNum = myScanner.hasNextInt();                //hasNextInt() is used to verify whether the user inputted an integer or not
      //if the user inputs other types, it will not accept the number
      
      if(courseNum == true){
        
        int x = myScanner.nextInt();     //storing the value for the course number
        
      }
      else{
        System.out.println("Invalid");  //if the user inputs other types, it will display invalid
        myScanner.next();
        
      }
      //it repeats itself for the rest of the code
      System.out.println("Enter the department's name"); //for the department number
      depName = myScanner.hasNext();   //hasNext() checking for whether it's a string or not
      
      if(depName == true){
        
        String depNamea = myScanner.next();   //stores for the department number
        
      }
      else{
        System.out.println("Invalid");  
        myScanner.next();
        
      }
      
      System.out.println("the number of times a week you attend that class");   //for the number of times the user attends the class
      numTimes = myScanner.hasNextInt();
      
      if(numTimes == true){
        
        int numTimesa = myScanner.nextInt();
      }
      else{
       System.out.println("Invalid");
        myScanner.next();
      }
      
      System.out.println("Enter the instructor's name");     //for the instructor's name
      instName = myScanner.hasNext();
      if(instName == true){
        String instNamea = myScanner.next();
      }
      else{
        System.out.println("Invalid");
        myScanner.next();
        
      }
      
      System.out.println("Enter the number of students attending the class");   //for the number of students the class has
      numStudents = myScanner.hasNextInt();
      if(numStudents == true){
        int numStudenta = myScanner.nextInt();
      }
      else{
        System.out.println("Invalid");
        myScanner.next();
      }
      System.out.println("Enter the time it starts to the nearest hour");
      timeStarts = myScanner.hasNextInt();
      
      if(timeStarts == true){
        int timeStartsa = myScanner.nextInt();
      }   
      else{
        System.out.println("Invalid");
        myScanner.next();
      }
      //for the second loop and next others, we use the same question that we asked the user earlier
       System.out.println("Enter 1 to input the numbers or 2 to quit");  //asking the user whether to input the values or to quit
       inputNum = myScanner.nextInt();
     
    
      
      
      
      
      
      
      
    }//end of while loop;
    
    
    
    
    
    
  }//end of main method
  
}//end of class
