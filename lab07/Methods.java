//Mickias Bekele; September 7, 2018; CSE02
//HW 06 EncryptedX.java 
/*The purpose of the program to generate a paragraph with the number of sentences the user wants to by using methods*/
import java.util.Scanner; //importing Scanner object or otherwise compiler error
import java.util.Random; //importing Random object or otherwise compiler error
public class Methods{
  //main method required for every Java program
    public static void main(String [] args){
    Scanner myScanner = new Scanner(System.in); //assigning scanner variable
    System.out.println("How many Sentences do you want ");
    int b = myScanner.nextInt(); //assigning a variable that stores the number the user enters
    conclusionSentence(b);  //calling conclusionSentence method
    }//end of main method
   public static void adjectives(int x){  //adjectives method
      //we create switch method to print out the adjectives
     switch(x){  
      case 0: System.out.print("The good "); //it generates random adjectives
      break;
      case 1: System.out.print("The quick ");
      break;
      case 2: System.out.print("The brown ");
      break; 
      case 3: System.out.print("The stupid ");
      break;
      case 4: System.out.print("The black ");
      break;
      case 5: System.out.print("The fast ");
      break;
      case 6: System.out.print("The green ");
      break;
      case 7: System.out.print("The grey ");
      break;
      case 8: System.out.print("The blue ");
      break;
      case 9: System.out.print("The white ");
      break;              
    }//end of adjectives method    
  }
  public static void Noun(int y){  //noun method
    switch(y){ //the same applies here for the noun
      case 0: System.out.print("fox "); 
      break;
      case 1: System.out.print("computer ");
      break;
      case 2: System.out.print("phone ");
      break;
      case 3: System.out.print("car ");
      break;
      case 4: System.out.print("motorcycle ");
      break;
      case 5: System.out.print("shoes ");
      break;
      case 6: System.out.print("hat ");
      break;
      case 7: System.out.print("bottle ");
      break;
      case 8: System.out.print("sock ");
      break;
      case 9: System.out.print("cloth ");
      break;
        
    
    }
  }//end of noun method
  public static void PastTense(int z){ //PastTense method
    switch(z){
      case 0: System.out.print("died of ");
      break;
      case 1: System.out.print("ran into ");
      break;
      case 2: System.out.print("played with ");
      break;
      case 3: System.out.print("killed the ");
      break;
      case 4: System.out.print("danced with ");
      break;
      case 5: System.out.print("slept ");
      break;
      case 6: System.out.print("took a shower ");
      break;
      case 7: System.out.print("walk by ");
      break;
      case 8: System.out.print("bought ");
      break;
      case 9: System.out.print("shiited ");
      break;
      
    }
  
  }//end of PastTense method
  public static void objectNoun(int a){  //objectNoun method
    switch(a){ 
      case 0: System.out.print("lazy dog. ");
      break;
      case 1: System.out.print("computer ");
      break;
      case 2: System.out.print("girl. ");
      break;
      case 3: System.out.print("wall. ");
      break;
        case 4: System.out.print("hair. ");
      break;
      case 5: System.out.print("window. ");
      break;
      case 6: System.out.print("bar. ");
      break;
      case 7: System.out.print("fence. ");
      break;
      case 8: System.out.print("key. ");
      break;
      case 9: System.out.print("earth surface. ");
      break;                              
   }  
  }//end of objectNoun method
  public static void firstSentence(int c){ //firstSentence method
    Random randomGenerator = new Random();  //assigning random variable
    int randomInt = randomGenerator.nextInt(10);  //assigning an integer variable that stores a randomly generated number between 0-9
    
    adjectives(randomInt); //calling adjectives method assigning the integer 
    Noun(randomInt); //calling noun method assigning the integer
    PastTense(randomInt); //calling PastTense method assigning the integer
    objectNoun(randomInt); //calling objectNoun assigning the integer
    
    }//end of firstSentence method
   
  
  public static void sentence(int d){ //sentence method
    Random random = new Random(); //assigning random variable 
    int y = random.nextInt(10); //assigning an integer that stores a randomly generated number between 0-9
    int c = 1;  //assigning an integer to 1
    firstSentence(c);  //calling the firstSentence
    String word = "It ";  //assigning a string to the word It
    int j = 1;  //assigning an integer to 1
    while(j <= d){ //while loop to generate the sentence
    System.out.print(word); //outputting the string
    PastTense(y); //calling PastTense method
    objectNoun(y); //calling objectNoun method
    j++; //incrementing 
    }
    
  }//end of sentence method
  public static void conclusionSentence(int l){ //conclusionSentence method
    Random randomGenerator2 = new Random(); //assigning a random generator
    int rand = randomGenerator2.nextInt(10); //assinging a integer that stores the randomly generated number between 0-9
    int d = l - 1; //initializing a variable and assigning it
    sentence(d); //calling sentence method
    System.out.print("That "); //outputting a word
    Noun(rand); //calling Noun method
    PastTense(rand); //calling PastTense method
    objectNoun(rand); //calling objectNoun method
  
  }//end of conclustionSentence method
  
  
  
  
  
  
}//end of class