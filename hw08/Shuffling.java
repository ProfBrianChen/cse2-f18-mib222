//Mickias Bekele; November 15, 2018; CSE02
//Lab 05 HW08.java
/*The purpose of the program is to ask the user to print out a whole deck of cards, shuffle them and print out the hand using arrays*/

//import objects or otherwise it will compile erro
import java.util.Scanner; //import scanner object
import java.util.Random; //import Random object
public class Shuffling{ //Shuffling class
  //main method required for every java program
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); //declaring a scanner variable
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    //assigning an array of strings for the suits name
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //assigning an array of strings for the cards
String[] cards = new String[52];  //assigning a new array of strings that has 52 elements in it

int again = 1; 
int index = 51;  //an integer for index
for (int i=0; i<52; i++){ //for loop for putting together the cards and suits
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
   
}//end of for loop
System.out.println();
printArray(cards); //calling the printArray method to print the whole deck of cards
System.out.println();
System.out.println();
shuffle(cards);  //calling the shuffle method to shuffle the cards and print them out
System.out.println();
int numCards = 0;  
String[] hands = new String[numCards];  //this array is for the hand
while(again == 1){
  if(index == 0){ //if statem the number of cards taken and the index are equal, then no card availiable
    System.out.println("Sorry out of cards. You have taken them all");
  }//end of if statement
  System.out.println("Enter how many cards you want in a hand");  //asking the user for how many cards wanted in a hand
  numCards = scan.nextInt();  //assinging the numCards to the number the user enters
  while(numCards > cards.length){  //while loop if the user inputted a number out of the range of 52 cards
    System.out.println("The number of cards you wanted is greater than 52. Enter again");  //try again
    scan.nextLine();
    numCards = scan.nextInt();
   }//end of while statement
  
  for(int i = 0; i < numCards; ++i){  //for loop for generating the hand
   hands = getHand(cards,index,numCards);  //calling the getHand and assigning the returned the array to hands Array
   System.out.print(hands[i] + " "); //printing out the hand
    }
  System.out.println();
  index = index - numCards; //we decrease this every time the user enters the number of cards and the cards get smaller in number
  System.out.println("Enter a 1 if you want another hand drawn");  //asking the user if they want to get a hand
  again = scan.nextInt();
}//end of while loop

  }//end of main method
 
public static void printArray(String[] list){ //printArray method
  for(int i = 0; i < list.length; ++i){ //for loop printing out the array
    System.out.print(list[i] + " ");
  }//end of for loop
 
}//end of printArray method
public static void shuffle(String[] list){ //shuffle method
  Random randomInt = new Random(); //declaring a random variable
 
  for(int i = 0; i < list.length; ++i){ // for loop for shuffling the cards
    int random = randomInt.nextInt(52);  //assigning an integer for a random number between 0-51
    System.out.print(list[random] + " ");  //printing out the shuffled cards
  
  }//end of for loop
  

}//end of shuffle method
public static String[] getHand(String[] list, int index, int numCards){ //getHand method
  Random randomInt = new Random();  //declaring a random varible
 
  String[] hands = new String[index + 1]; //assigning a string to the with the number of elements equal to the index plus 1
  for(int i = 0; i < numCards; ++i){ //for loop for getting the hand
    int random = randomInt.nextInt(index + 1); //assigning a random integer to the number of index plut 1
    hands[i] = list[random]; //storing the hand
    
  }//end of for loop
 return hands; //returning the hand

}//end of getHand method



}//end of shuffling class



