//Mickias Bekele; September 7, 2018; CSE02
//HW 03 Pyramid.java
/*The purpose of the program is generate or to make the user input values for the rolling of two dices (craps), and to output the slang terminology 
 for describing the outcome of the roll of two dices*/

//use first import method otherwise the compiler will generate error messages.
import java.util.Scanner; //use Scanner if the user wants to input the roll of the two dices
import java.util.Random;  //use Random if the user wants the computer to generate the number
//use public class
public class CrapsSwitch{
  //main method is required for every Java program
    public static void main(String[] args){
      Scanner myScanner = new Scanner(System.in);
      int userDependent; //input variable for the user to choose between generating random numbers or inputting the value for both dices
      String slangTerm = "Error"; //output variable for the slang terminology 
      // we initialize the variable to avoid compiler error
      int rollDiceOne;  //input variale for the value of the first dice
      int rollDiceTwo;  //input variable for the value of the second dice
      
      System.out.println("Enter 1 if you want to randomly cast a dice or 2 if you input the dice"); //asks the user to choose between generating numbers for the dice or inputting the value for the dices
      userDependent = myScanner.nextInt(); 
      switch(userDependent){     //use switch for the conditionals
        case 1:                               //choice for the user to randomly generate the dices
          Random randGen = new Random();      //declaring random variable
          rollDiceOne = randGen.nextInt(6) + 1;  //assigning the first dice to randomly generate numbers from 1 to 6 inclusive
          rollDiceTwo = randGen.nextInt(6) + 1;  //assigning the second dice to randomly generate numbers from 1 to 6 inclusive
          //we are going to use switch for the conditions
          switch(rollDiceOne){                 //switch statement for the first dice
            case 1:                            //if the dice is 1
              //we are going to use switch statements for the second dice for every number that the first dice generated
              switch(rollDiceTwo){             // switch statement for the second dice
                //for every case, we are going to assign the slangTerm the terminology
                case 1: slangTerm = "Snake Eyes";  // For example, the first dice is 1 and second dice is 1 also, so the slang term will be Snake Eyes
                  break;                           //We use breaks to finish the first condition and transit to the second one
                case 2: slangTerm = "Ace Deuce";   //The same thing happens here
                  break;
                case 3: slangTerm = "Easy Four";
                  break;
                case 4: slangTerm = "Fever Five";
                  break;
                case 5: slangTerm = "Easy Six";
                  break;
                case 6: slangTerm = "Seven Out";
                  break;
                default: System.out.println("Invalid"); //We use default in case the computer accidentally generated a number out of range (1-6 inclusive)
                  break;
                }
            case 2:                               //if dice 1 is 2 
              switch(rollDiceTwo){
                case 1: slangTerm = "Ace Deuce";
                  break;
                case 2: slangTerm = "Hard Four";
                  break;
                case 3: slangTerm = "Fever Five";
                  break;
                case 4: slangTerm = "Easy Six";
                  break;
                case 5: slangTerm = "Seven Out";
                  break;
                case 6: slangTerm = "Easy Eight";
                  break;
                default: System.out.println("Invalid");
                  break;
            }
              //rest of the code follows as the above outlined
            case 3: 
              switch(rollDiceTwo){
                case 1: slangTerm = "Easy Four";
                  break;
                case 2: slangTerm = "Fever Five";
                  break;
                case 3: slangTerm = "Hard Six";
                  break;
                case 4: slangTerm = "Seven out";
                  break;
                case 5: slangTerm = "Easy Eight";
                  break;
                case 6: slangTerm = "Nine";
                  break;
                default: System.out.println("Invalid");
                  break;
                 }
            case 4: 
              switch(rollDiceTwo){
                case 1: slangTerm = "Fever Five";
                  break;
                case 2: slangTerm = "Easy Six";
                  break;
                case 3: slangTerm = "Seven Out";
                  break;
                case 4: slangTerm = "Hard Eight";
                  break;
                case 5: slangTerm = "Nine";
                  break;
                case 6: slangTerm = "Easy Ten";
                  break;
                default: System.out.println("Invalid");
                  break;
              }
            case 5: 
              switch(rollDiceTwo){
                case 1: slangTerm = "Easy Six";
                  break;
                case 2: slangTerm = "Seven Out";
                  break;
                case 3: slangTerm = "Easy Eight";
                  break;
                case 4: slangTerm = "Nine";
                  break;
                case 5: slangTerm = "Hard Ten";
                  break;
                case 6: slangTerm = "Yo-leven";
                  break;
                default: System.out.println("Invalid");
                  break;
              }
            case 6: 
              switch(rollDiceTwo){
                case 1: slangTerm = "Seven out";
                  break;
                case 2: slangTerm = "Easy Eight";
                  break;
                case 3: slangTerm = "Nine";
                  break;
                case 4: slangTerm = "Easy Ten";
                  break;
                case 5: slangTerm = "Yo-leven";
                  break;
                case 6: slangTerm = "Boxcars";
                  break;
                default: System.out.println("Invalid");
                  break;
            }
        }
        //Print out the slang terminology
          System.out.println("The score is " + slangTerm);                             //outputting the variable for the slang terminology
            break;
          
        case 2: //second choice for the user to input the number of both dice
          System.out.println("Enter the value for the first dice");   //asking the user to input for the first dice     
          rollDiceOne = myScanner.nextInt();
          System.out.println("Enter the value for the second dice"); //asking the user to input for the second dice
          rollDiceTwo = myScanner.nextInt();
                
          switch(rollDiceOne){                              //The same thing happen here as the one before (in the first case)
            case 1: 
              switch(rollDiceTwo){
                case 1: slangTerm = "Snake Eyes";
                  break;
                case 2: slangTerm = "Ace Deuce";
                  break;
                case 3: slangTerm = "Easy Four";
                  break;
                case 4: slangTerm = "Fever Five";
                  break;
                case 5: slangTerm = "Easy Six";
                  break;
                case 6: slangTerm = "Seven Out";
                  break;
                default: System.out.println("Invalid");
                  break;
           }
              break;
            case 2:
               switch(rollDiceTwo){
                case 1: slangTerm = "Ace Deuce";
                  break;
                case 2: slangTerm = "Hard Four";
                  break;
                case 3: slangTerm = "Fever Five";
                  break;
                case 4: slangTerm = "Easy Six";
                  break;
                case 5: slangTerm = "Seven Out";
                  break;
                case 6: slangTerm = "Easy Eight";
                  break;
                default: System.out.println("Invalid");
                  break;
         }
              break;
            case 3: 
              switch(rollDiceTwo){
                case 1: slangTerm = "Easy Four";
                  break;
                case 2: slangTerm = "Fever Five";
                  break;
                case 3: slangTerm = "Hard Six";
                  break;
                case 4: slangTerm = "Seven out";
                  break;
                case 5: slangTerm = "Easy Eight";
                  break;
                case 6: slangTerm = "Nine";
                  break;
                default: System.out.println("Invalid");
                  break;
           }
              break;
            case 4: 
               switch(rollDiceTwo){
                case 1: slangTerm = "Fever Five";
                  break;
                case 2: slangTerm = "Easy Six";
                  break;
                case 3: slangTerm = "Seven Out";
                  break;
                case 4: slangTerm = "Hard Eight";
                  break;
                case 5: slangTerm = "Nine";
                  break;
                case 6: slangTerm = "Easy Ten";
                  break;
                default: System.out.println("Invalid");
                  break;
              }
              break;
            case 5: 
              switch(rollDiceTwo){
                case 1: slangTerm = "Easy Six";
                  break;
                case 2: slangTerm = "Seven Out";
                  break;
                case 3: slangTerm = "Easy Eight";
                  break;
                case 4: slangTerm = "Nine";
                  break;
                case 5: slangTerm = "Hard Ten";
                  break;
                case 6: slangTerm = "Yo-leven";
                  break;
                default: System.out.println("Invalid");
                  break;
              }
              break;
            case 6:
              switch(rollDiceTwo){
                case 1: slangTerm = "Seven out";
                  break;
                case 2: slangTerm = "Easy Eight";
                  break;
                case 3: slangTerm = "Nine";
                  break;
                case 4: slangTerm = "Easy Ten";
                  break;
                case 5: slangTerm = "Yo-leven";
                  break;
                case 6: slangTerm = "Boxcars";
                  break;
                default: System.out.println("Invalid");
                  break;
           }
              break;
            default: System.out.println("Invalid entry");  //We use default incase the user enters a number out of the range 1-6 inclusive
              break;
          }
          //Print out the output
        System.out.println("The score is " + slangTerm);   //outputting the value for the slang terminology
            break;
        default: System.out.println("Invalid");  //we use this incase the user enters a number out of the number they've been asked
          break;
      }
      
   }//end of main method
  
  }//end of class