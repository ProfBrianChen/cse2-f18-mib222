//Mickias Bekele; September 7, 2018; CSE02
//HW 03 Pyramid.java
/*The purpose of the program is generate or to make the user input values for the rolling of two dices (craps), and to output the slang terminology 
 for describing the outcome of the roll of two dices*/

//use first import method otherwise the compiler will generate error messages.
import java.util.Scanner; //use Scanner if the user wants to input the roll of the two dices
import java.util.Random;  //use Random if the user wants the computer to generate the number

public class CrapsIf{
  //main method is required for every Java program
    public static void main(String[] args){
       Scanner myScanner = new Scanner(System.in); //assigning scanner variable
      int userDependent;    //input variable for the user to choose between generating random numbers or inputting the value for both dices
      String slangTerm = "Error";  //output variable for the slang terminology 
      // we initialize the variable to avoid compiler error
      int rollDiceOne; //input variale for the value of the first dice
      int rollDiceTwo; //input variable for the value of the second dice
      
      System.out.println("Enter '1' if you want to randomly cast a dice or '2' if you input the dice"); //asks the user to choose between generating numbers for the dice or inputting the value for the dices
      userDependent = myScanner.nextInt();
       //we use if statements for the condition
      if(userDependent == 1) {  //choice for the user to randomly generate the dices
        Random randGen = new Random();              //declaring random variable
          rollDiceOne = randGen.nextInt(6) + 1;     //assigning the first dice to randomly generate numbers from 1 to 6 inclusive
          rollDiceTwo = randGen.nextInt(6) + 1;     //assigning the second dice to randomly generate numbers from 1 to 6 inclusive
          //we are going to use if statement for the conditions
          if(rollDiceOne == 1){           //if statement for the first dice
             //we are going to use if statements for the second dice for every number that the first dice generated  
            if(rollDiceTwo == 1){  //if statements for the second dice
              //For every if statements, we are going to assign the slang term its terminology
                slangTerm = "Snake Eyes";   //For example the first dice here is 1 and second one is 1, so the slang term is Snake Eyes.             
              }     //finish the if statements with curly braces
              else if(rollDiceTwo == 2){    //The same thing happens here
                slangTerm = "Ace Deuce";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Easy Four";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Fever Five";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Easy Six";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Seven out";
              }
              else{
                slangTerm = "Invalid";   //We use in case the computer generates a number out of the range 1-6 inclusive
              }
          }
          else if(rollDiceOne == 2){    //if dice one is 2
            if(rollDiceTwo == 1){
                slangTerm = "Ace Deuce";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Hard Four";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Fever Five";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Easy Six";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Seven Out";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Easy Eight";
              }
              else{
                slangTerm = "Invalid";
              } 
            //rest of the code follows as outlined above
          }
          else if(rollDiceOne == 3){
            if(rollDiceTwo == 1){
                slangTerm = "Easy Four";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Fever Five";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Hard Six";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Seven out";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Easy Eight";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Nine";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if (rollDiceOne == 4){
            if(rollDiceTwo == 1){
                slangTerm = "Fever Five";               
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Easy Six";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Seven out";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Hard Eight";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Nine";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Easy Ten";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if(rollDiceOne == 5){
            if(rollDiceTwo == 1){
                slangTerm = "Easy Six";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Seven out";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Easy Eigth";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Nine";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Hard Ten";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Yo-leven";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if(rollDiceOne == 6){
            if(rollDiceTwo == 1){
                slangTerm = "Seven out";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Easy Eight";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Nine";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Easy Ten";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Yo-leven";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Boxcars";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else{
            slangTerm = "Invalid"; //incase the computer generates number out of the range 1-6 inclusive
          }
          //Print out the output
          System.out.println("The score is " + slangTerm);    //outputting the slang terminology
        
        
        
      }
        if(userDependent == 2) {  //second choice for the user to input for both dice
          
          System.out.println("Enter value for the first dice");   //asking the user for the first dice
          rollDiceOne = myScanner.nextInt();
          System.out.println("Enter value for the second dice");  //asking the user for the second dice
          rollDiceTwo = myScanner.nextInt();
          
          if(rollDiceOne == 1){  //the same things happens as outlined above in the first if statement
            
            if(rollDiceTwo == 1){
                slangTerm = "Snake Eyes";     //first dice is 1 and second dice is 1 so the output will be Snake Eyes           
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Ace Deuce";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Easy Four";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Fever Five";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Easy Six";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Seven out";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if(rollDiceOne == 2){
            if(rollDiceTwo == 1){
                slangTerm = "Ace Deuce";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Hard Four";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Fever Five";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Easy Six";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Seven Out";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Easy Eight";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if(rollDiceOne == 3){
            if(rollDiceTwo == 1){
                slangTerm = "Easy Four";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Fever Five";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Hard Six";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Seven out";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Easy Eight";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Nine";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if (rollDiceOne == 4){
            if(rollDiceTwo == 1){
                slangTerm = "Fever Five";               
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Easy Six";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Seven out";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Hard Eight";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Nine";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Easy Ten";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if(rollDiceOne == 5){
            if(rollDiceTwo == 1){
                slangTerm = "Easy Six";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Seven out";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Easy Eigth";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Nine";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Hard Ten";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Yo-leven";
              }
              else{
                slangTerm = "Invalid";
              }
          }
          else if(rollDiceOne == 6){
            if(rollDiceTwo == 1){
                slangTerm = "Seven out";                
              }
              else if(rollDiceTwo == 2){
                slangTerm = "Easy Eight";
              }
              else if(rollDiceTwo == 3){
                slangTerm = "Nine";
              }
              else if(rollDiceTwo == 4){
                slangTerm = "Easy Ten";
              }
              else if(rollDiceTwo == 5){
                slangTerm = "Yo-leven";
              }
              else if(rollDiceTwo == 6){
                slangTerm = "Boxcars";
              }
              else{
                slangTerm = "Invalid";  //incase the user inputs a number out of range 1-6 inclusive for the second dice
              }
          }
          else{
            slangTerm = "Invalid";    //incase the user inputs a number out of the range 1-6 inclusive for the first dice
          }
          //Print out 
          System.out.println("The score is " + slangTerm);  //outputting the slang terminology
        
        
        
      }
      else{
        System.out.println("Wrong number. Enter again '1' for randomly generating numbers or '2' for inputting numbers");
      }
          
        
      
      
      
    }//end of main method
  
  
  
}//end of class