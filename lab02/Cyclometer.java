//Mickias Bekele; September 7, 2018; CSE02
//The purpose of the program is:/* to print the number of minutes for each trip
                                /*print the number of counts for each trip
                                /*print the distance of each trip in miles
                                /*print the distance for the two trips combined*/




public class Cyclometer{
  
  //main method is required for every Java program
  
  public static void main(String args[]){
    
    //our input data with variables
    
    int secsTrip1 = 480;     //input value for the number of seconds for the first trip
    int secsTrip2 = 3220;    //input value for the number of seconds for the second trip
    int countsTrip1 = 1561;  //input value for the number of counts for the first trip
    int countsTrip2 = 9037;  //input value for the number of counts for the second trip
    
    //our intermediate variables and output variables
    
    double wheelDiameter=27.0;  //intermediate variable for the diameter of the wheel
  	double PI = 3.14159;        //value used for pi
  	double feetPerMile=5280;    //intermediate variable for the number of feets found in a mile
  	double inchesPerFoot=12;    //value for the number of inches found in a foot
  	double secondsPerMinute=60; //value for the number of seconds found in a minute
	  double distanceTrip1, distanceTrip2, totalDistance;  //output variables 
    distanceTrip1 = (countsTrip1 * wheelDiameter * PI);
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1 =  (countsTrip1 * wheelDiameter * PI) / (inchesPerFoot * feetPerMile); // Gives distance in miles
	  distanceTrip2 = (countsTrip2 * wheelDiameter * PI) / (inchesPerFoot * feetPerMile); // Gives the the distance in the second trip in miles
    totalDistance = distanceTrip1 + distanceTrip2;
	
    
    //print out the output value
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) +" minutes and had "+  //outputing the results found in the number of minutes and number of counts found in the first trip
       	      countsTrip1 +" counts.");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute)+" minutes and had " + countsTrip2+" counts."); //ouputing the results for the number of minutes and number of counts for the second time
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); //output the distance for the first trip
	  System.out.println("Trip 2 was " + distanceTrip2 + " miles"); //output the distance for the second trip
	  System.out.println("The total distance was " + totalDistance +" miles"); //output for the total distance

    
  } //end of main method
} //end of class