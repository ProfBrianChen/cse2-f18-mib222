//Mickias Bekele; September 7, 2018; CSE02
//The purpose of this program is to demonstrate how you can get input from the user and use that data to perform basic computations.
//We do this by: /*Write a program that uses the Scanner class to obtain from the user the original cost of the check
                /*he percentage tip they wish to pay, and the number of ways the check will be split.
                /*Then determine how much each person in the group needs to spend in order to pay the check. */





import java.util.Scanner; //you must first import it otherwise the compiler will generate error messages.


public class Check{
  
  //main method required for every Java program
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in); //in order to accept input, you must declare an instance of the Scanner object and call the Scanner constructor.
    System.out.print("Enter the original cost of the check in the form of (xx.xx)"); //asking the user to input the original cost of the check
    
    double checkCost = myScanner.nextDouble(); //input variable for the original cost

    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx)"); //asking the user to input the percentage tip of their original cost
    double tipPercent = myScanner.nextDouble(); //input variable for the tip percentage
    
    tipPercent /= 100;
    
    System.out.print("Enter the number of people who went out to dinner:"); //asking the user to input the number of people that split the check equally
    int numPeople = myScanner.nextInt(); //input variable for the number of people
    
    //Use all the input variables and assign variables to calculate each person's cost
    
    double totalCost; //variable assigned for the total check cost
    double costPerPerson; //variable assigned for the cost of each person owe
    int dollars;      //whole dollar amount of cost 

    int dimes, pennies;       //for storing digits to the right of the decimal point for the cost$ 

   
    totalCost = checkCost * (1 + tipPercent); //getting the total cost by using the tip percent
    costPerPerson = totalCost / numPeople; //calculating the cost each person pay
    
    dollars = (int) costPerPerson; //get the whole amount in dollars, dropping decimal fraction

    dimes = (int) (costPerPerson * 10) % 10; ////get dimes amount, e.g., (int)(6.73 * 10) % 10 -> 67 % 10 -> 7, where the % (mod) operator returns the remainder


    pennies = (int) (costPerPerson * 100) % 10; //after the division:   583%100 -> 83, 27%5 -> 2 
    
    System.out.println("Each person in the group owe $" + dollars + '.' + dimes + pennies); //print out the outpu
    
    myScanner.nextLine();
    
    boolean answerQuestion = 2 + 5 > 7 - 4 == 'a' > 'b';
    
    System.out.println(answerQuestion);
    



    
    
  }//end of main method
  
}//end of class