//Mickias Bekele; November 27, 2018; CSE02
// HW09.java
/*The purpose of the program is to delete and remove elements from an array*/
//import objects or compile error
import java.util.Scanner; //importing Scanner
import java.util.Random; //importing Random
public class RemoveElements{ //class RemoveElements
  //main method required for every Java program
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in); //declaring scanner variable
int num[]=new int[10]; //declaring an arry with 10 index
int newArray1[]; // initializing array
int newArray2[]; //initializing array
int index,target; //initializing integers
String out1 = " "; //initializing string out1
String out2 = " "; //initializing string out2


 String answer=""; //initializing string
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput(); //randomly inputs elements
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out); 
   System.out.print("Enter the index ");
   index = scan.nextInt(); //stores the index
   newArray1 = delete(num,index); //
   
   out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1); //outputs the new array
   System.out.println("Enter the target value ");
   target = scan.nextInt();  //stores the target
   newArray2 = remove(num,target); //outputs the new array
   out2="The output array is ";
   out2 +=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2); //outputs a new array
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y")); //end of do-while method
  }//end of main method
  
  public static int[] randomInput(){ //randomInput method
    Random rand = new Random();
    int randomInt;
    int[] array1 = new int[10];
    for(int i = 0; i < array1.length; i++){
      randomInt = rand.nextInt(10);
      array1[i] = randomInt;
    }//end of for loop
    return array1;
  }//end of randomInpu
  public static int[] delete(int[] list, int pos){ //delete method
    int[] array2 = new int[9];
    int a;
    for(int i = 0; i < list.length - 1; i++){
      
      if(i >= pos){
        array2[i] = list[i+1];;
        
      }//end of if statement
      else{
        array2[i] = list[i];
      }//end of else statemet
      
    }//end of for loop
    
    return array2;
  }//end of delete method
  public static int[] remove(int[] list, int target){ //remove method
    
    int[] array4 = null;
    int sum = 0;
    for(int i = 0; i < list.length; ++i){
      if(list[i] == target){
        sum++;
      }//end of if statement
      
    }//end of for loop
    
    array4 = new int[list.length - sum]; //assigns the new array a new index
   
    int key = 0;
    for(int i = 0; i < list.length - sum; i++){
        
      if(list[i] != target){
        array4[i - key] = list[i];
      }//end of if statement
      else{
       key++;
       
           }
    }//end of for loop
      return array4;
  }//end of remove method
 
 public static String listArray(int num[]){ //listArray method
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out; //returns the array to main method
  }//end of list Array method
}//end of class
