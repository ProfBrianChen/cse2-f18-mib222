//Mickias Bekele; November 27, 2018; CSE02
// HW08.java
/*The purpose of the program is to finding the a key and its index in array*/
//import object or it will result in compiler error
import java.util.Scanner; //import scanner object
import java.util.Random; //import random object
public class CSE2Linear{
  //main method for every Java program
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in); //declaring scanner variable
    Random rand = new Random(); //assining a new random variable
    int randomNumber; 
    final int NUM_ELEMENTS = 15;
    int[] number = new int[NUM_ELEMENTS];
    int[] number2 = new int[NUM_ELEMENTS];
    int[] number3 = new int[NUM_ELEMENTS];
    boolean checkNumber = true; //boolean for checking whether it is integer or not
    int b = 1;
    int key;
    String out;
    int keyIndex;
    System.out.println("Enter number (0-100): ");
    for(int i = 0; i < NUM_ELEMENTS; ++i){ //for loop
    checkNumber = myScanner.hasNextInt();
    while(checkNumber == false){ //check whether or not it is integer or not
      System.out.println("The number inputted is not an integer. Enter again: ");
      myScanner.next();
      checkNumber = myScanner.hasNextInt();
    }//end of while loop
    number[i] = myScanner.nextInt();
    while(number[i] > 100 || number[i] < 0){ //checks the range
      System.out.println("Enter again. The inputted number is out of rang: ");
      myScanner.next();
      number[i] = myScanner.nextInt();
    }//end of while loop
    number2[i] = number[i];
    if(i >= 1){
    while(number[i-1] > number[i]){ //checks whether the number is bigger or not
      System.out.println("Enter again. The number entered now is less than the number entered before: ");
      myScanner.next();
      number[i] = myScanner.nextInt();
    }//end of while loop
    number2[i] = number[i];
    
    }//end of if statement
    }//end of for lo
    
    for(int j = 0; j < NUM_ELEMENTS; ++j){
      System.out.print(number2[j] + " ");
    }//end of for loop
    
    System.out.print(" Enter a grade to search for: ");
    key = myScanner.nextInt(); //stores the key
    
    out = binarySearch(key, number2); //calls the binary search 
    System.out.println(key + out); //outputs the grade
    
    System.out.println("Scrambled: ");
    myScanner.nextLine();
    
    for(int a = number.length - 1; a >= 0; --a){ //for loop for scrambling 
      randomNumber = rand.nextInt(a+1);
      b = number2[randomNumber]; 
      number2[randomNumber] = number2[a];
      number2[a] = b;
      System.out.print(number2[a] + " ");
    }//end of for loop
   
    
    System.out.println("Enter a grade to search for: ");
    key = myScanner.nextInt();
    keyIndex = linearSearch(key, number2); //linear search for grade
    keyIndex = 14 - keyIndex;
    if(keyIndex == -1){
      System.out.println(key + "is not found");
      
    }//end of if statement
    else{
      System.out.println(key +" found at " + keyIndex + " in the list with " + keyIndex + " iterations."); //outputs the key and its index with interation
    }//end of else statement
    
  }//end of main method
  
  public static String binarySearch(int key, int[] number2){ //binarySearch
    String out = "";
    int low = 0;
    int high = number2.length - 1;
    int mid;
    int i=1;
   while(high >= low){ //while loop for finding the index
      mid = (high + low) / 2;
      i++;
      if(number2[mid] < key){
        low = mid + 1;
      }//end of if 
      if(number2[mid] > key){
        high = mid -1;
      }//end of if statement
      if(number2[mid] == key){
        out = " was found at index " + mid + " in the list with " + i + " iterations."; 
        return out; //returns the key with its iterations
            
        
      }//end of else statement
      
      
    }//end of for loop
       
    out = "was not found";
    return out; //returns the string
    
  
  }//end of binarySearch method
  
  public static int linearSearch(int key, int[] number3){ //linearSearch
    int i;
    for(i = 0; i < number3.length; ++i) //for loop for searching the key
      if(number3[i] == key){
        return i; //returns the index
      }//end of if statement
    }//end of for loop 
    return -1;
  }//end of linearSearch 


}//end of class