//Mickias Bekele; September 7, 2018; CSE02

//HW 03 Pyramid.java

//The purpose of the program is to calculate the volume of a pyramid


 //use first import method otherwise the compiler will generate error messages.

import java.util.Scanner;

public class Pyramid{
  //main method required for every Java method
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in);                               //we use this to make the user input values for the height and square side of the pyramid
    System.out.print("Enter the length of the square side of the pyramid: "); //asking the user to input the values for the length of square side of the pyramid
    
    double squareSide = myScanner.nextDouble();                               //input variable for the length of square side of the pyramid
    
    System.out.print("Enter the height of the pyramid: ");                    //asking the user to input for the height of the pyramid
    
    double heightPyramid = myScanner.nextDouble();                            //input variable for the height of the pyramid
    
    double volumeOfPyramid;                                                   //output variable for the volume of the pyramid
      
    volumeOfPyramid = (Math.pow(squareSide, 2) * heightPyramid) / 3;          //calculating the volume of the pyramid from the values entered by the user
    
    //print out the output
    
    System.out.println("The volume inside the pyramid from the given measurement is " + volumeOfPyramid); //output for the volume of the pyramid
    
    
    
    
    
  }//end of main method
  
  
}//end of class

