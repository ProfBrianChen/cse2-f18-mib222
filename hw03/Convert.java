//Mickias Bekele; September 7, 2018; CSE02

//HW 03 Convert.java

//The purpose of this program is to calculate the quantity of rain affected by the hurricane in cubic miles.


//use first the import method or otherwise the compiler will generate error messages

import java.util.Scanner;

public class Convert{
// main method is required for every Java program
  
  public static void main(String args[]){
      
     Scanner scnr = new Scanner(System.in);                                           //we use this to make the user input values of the number of inches of the rainfall and acre of land affected by the hurricane
     
     System.out.print("Enter the affected area in acres ");                           //Asking the user to enter the value for the acre of land
    
     double acresOfLand = scnr.nextDouble();                                          //input variable for the acres of land affected by the hurricane
    
     System.out.print("Enter the amount of rainfall in the affected area in inches "); //Asking the user to enter the value for the number of inches of rainfall in the affected area
     
     double rainInInches = scnr.nextDouble();                                          //input variable for the number of inches of the rainfall
    
    
    
     double cubicMiles = (rainInInches * acresOfLand) * 27154.285990761;               //output variable for the quantity of rain in cubic miles 
     //first in the equation above we converted acre-inch into gallon by multiplying it with a constant number.
     cubicMiles *= 9.08169e-13;                                                        //After converting into gallon, we then converted it into cubic miles by multiplying it with that number
     
     //print out the variable
    
     System.out.println("The quantity of rain affected by the hurricane in cubic miles is " + cubicMiles); //output for the quantity of rain in cubic miles
     
    
    
  }//end of main method
  
}//end of class
                                              
                                              