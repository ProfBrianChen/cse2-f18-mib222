//import or otherwise it will compile erro
import java.util.Random;//importing Random Object

public class Arrays{
  //main method required for every Java
  public static void main(String[] args){
    Random randomInt = new Random(); //assigning random variable
    final int NUM_ELEMENTS = 100;  //assinging an unchanging integer 
    int[] randomArray = new int[NUM_ELEMENTS];  //assinging the size of randomArray to NUM_ELEMENTS
    int[] numOccurrences = new int[NUM_ELEMENTS]; //assigning the size of numOccurrences to NUM_ELEMENTS
    int counter = 0, count = 0;
   
    
    for(int i = 0; i < randomArray.length; ++i){ //for loop for assinging random integers to random Array
       int randomInt2 = randomInt.nextInt(100);
       
       randomArray[i] = randomInt2;
      
    }//end of for loop
    for(int i = 0; i < randomArray.length; ++i){ //for loop for the counting the number of occurrences of repeating numbers
      count = randomArray[i];
      numOccurrences[count]++;
      
    }//end of for loop
    
    for(int i = 1; i < randomArray.length; ++i){ //for loop for printing out the output
      System.out.println(i + " occurs " + numOccurrences[i] + " times.");
    
    }//end of for loop
    
    
  }//end of main method



}//end of class